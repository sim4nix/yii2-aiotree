<?php

namespace sim4nix\aiotree;

use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

class AIOTree extends Widget
{
	const TYPE_LABEL = 'label';
	const TYPE_RADIO = 'radio';
	const TYPE_CHECKBOX = 'checkbox';

	/** * DATA * **/
	public $data = [];
	/** sample format
	 * array(
	 *    '1' => array('parentId' => '', 'text' => 'PARENT'),
	 *  '2' => array('parentId' => '2', 'text' => 'CHILD'),
	 * )
	 **/

	/** @var ActiveRecord */
	public $model;
	public $attribute = 'attr';//default

	/** * NAME */
	public $name;
	public $value;

	/** TYPES OF TREE **/
	public $type = self::TYPE_RADIO;//radio, checkbox

	/** only for checkbox **/
	public $selectParent = false;

	/**      * HEADER     **/

	/** header value show or hide **/
	public $headerShow = true;
	/** header text **/
	public $header;
	public $headerClass = 'aiotree_header';


	/** TREE CONTROL COLLAPSE EXPAND **/
	public $labelCollapse;
	public $labelCollapseId = 'aiotree_collapse';
	public $labelCollapseClass = 'aiotree_collapse';
	public $labelExpand;
	public $labelExpandId = 'aiotree_expand';
	public $labelExpandClass = 'aiotree_expand';

	/** set control params for collapse and expand**/
	public $controlShow = true;
	public $controlTag = 'div';
	public $controlClass = '';
	public $controlId = 'aioTree_control';
	public $controlStyle = '';
	public $controlLabel = [];//collapse, expand
	public $controlDivider = ' | ';
	public $controlHtmlOptions = [];

	/** set parent class **/
	public $parentShow = true;
	public $parentTag = 'div';
	public $parentClass = '';
	public $parentId = '';
	public $parentStyle = '';
	public $parentHtmlOptions = [];

	/** TREE UL CLASS AND STYLE **/
	public $treeClass = '';
	public $treeStyle = '';
	public $treeOptions = [];

	/** LI CLASS AND STYLE ID **/
	public $liListClass = '';
	public $liListId = '';
	public $liListStyle = '';
	public $liHtmlOptions = [];

	public $pluginOptions = [
		'collapsed' => true,
		'animated'  => 'medium',
		'persist'   => 'location'
	];

	public $allowNull = false;

	private $_html = '';

	/**
	 * @return mixed
	 */
	public function getValue()
	{
		$model     = $this->model;
		$attribute = Html::getAttributeName($this->attribute);

		return isset($model) && is_object($model) ? $model->$attribute : $this->value;
	}

	public function run()
	{
		if ($this->data && is_callable($this->data)) {
			$this->data = call_user_func($this->data, $this->model);
		}

		if (!$this->labelCollapse) {
			$this->labelCollapse = \Yii::t('app', 'Collapse all');
		}

		if (!$this->labelExpand) {
			$this->labelExpand = \Yii::t('app', 'Expand all');
		}

		$name = is_object($this->model) ? Html::getInputName($this->model, $this->attribute) : $this->name;
		$this->_html .= Html::hiddenInput($name, null, ['class' => 'kv-editable-input']);

		$id = is_object($this->model) ? Html::getInputId($this->model, $this->attribute) : '';
		$this->_html .= '<div id="' . $id . '">';
		$this->registerAsset();
		$this->parentStart();
		$this->showHeader();
		$this->renderControl();
		$this->renderList();
		$this->parentClose();
		$this->_html .= '</div>';

		return $this->_html;
	}

	/**
	 * Render list
	 */
	public function renderList()
	{
		/** ul open **/
		$this->_html .= '<ul id="' . $this->getId() . '"';
		$this->displayHtmlAttr($this->treeClass, 'class');
		$this->displayHtmlAttr($this->treeStyle, 'style');
		$this->treeOptions['id'] = '';
		$this->displayHtmlAttr($this->treeOptions);
		$this->_html .= '>';
		$this->displayList();

		/** ul end**/
		$this->_html .= '</ul>';
	}

	/**
	 * Display list
	 */
	public function displayList()
	{
		$listData = $parentList = [];

		foreach ($this->data as $key => $row) {
			if ($row['parentId'] == '') {
				$parentList[] = $key;
			} else {
				$listData[$row['parentId']][] = $key;
			}
		}
		foreach ($parentList as $parentId) {
			$this->makeTree($listData, $parentId);
		}
	}

	/**
	 * @param $listData
	 * @param $id
	 */
	public function makeTree($listData, $id)
	{
		if (isset($listData[$id]) && is_array($listData[$id])) {
			$this->renderLi($id);
			$this->_html .= '<ul>';

			foreach ($listData[$id] as $data) {
				$this->makeTree($listData, $data);
			}

			$this->_html .= '</ul>';
		} else {
			$this->renderLi($id);
		}
	}

	/**
	 * @param $id
	 */
	public function renderLi($id)
	{
		$this->_html .= '<li>';

		if ($this->type == static::TYPE_LABEL || $this->type == '') {
			$this->_html .= '<label';
			$this->displayHtmlAttr($this->liListClass, 'class');
			$this->displayHtmlAttr($this->liListId, 'id');
			$this->displayHtmlAttr($this->liListStyle, 'style');
			$this->displayHtmlAttr($this->liHtmlOptions);
			$this->_html .= '>';
			$this->_html .= $this->data[$id]['text'];
			$this->_html .= '</label>';
		} else if (in_array($this->type, [static::TYPE_RADIO, static::TYPE_CHECKBOX])) {
			$model     = $this->model;
			$attribute = Html::getAttributeName($this->attribute);
			$htmlId    = '';
			$name = $this->name ? $this->name : '';
			$value = $this->getValue();

			if (isset($model) && is_object($model)) {
				$htmlId .= Html::getInputId($model, $attribute) . '_' . md5(var_export($value, true));
				$name =  Html::getInputName($model, $this->attribute);
			}
			$htmlId .= '_' . $id;
			$name .= $this->type == static::TYPE_CHECKBOX ? '[]' : '';
			$this->_html .= '<label for="' . $htmlId . '">';
			$this->_html .= '<input type="' . $this->type . '" data-tree-' . $this->getId() . '="1" value="' . $id . '"';
			$this->_html .= ' name="' . $name . '" ';
			$this->displayHtmlAttr($this->liListClass, 'class');
			$this->displayHtmlAttr($this->liListId, 'id');
			$this->displayHtmlAttr($this->liListStyle, 'style');
			$this->displayHtmlAttr($this->liHtmlOptions);
			$this->_html .= ' id="' . $htmlId . '"';

			if (!empty($value) && ($value == $id || (is_array($value) && in_array($id, $value)))) {
				$this->_html .= ' checked="checked"';
			}
			if (is_object($model) && $id == $model->getId()) {
				$this->_html .= ' disabled="disabled"';
			}
			$this->_html .= ' id="' . $htmlId . '">';

			$this->_html .= $this->data[$id]['text'];
			$this->_html .= '</label>';
		}
	}

	/**
	 * Public js script
	 */
	public function registerAsset()
	{
		if (!isset($this->pluginOptions['control'])) {
			$this->pluginOptions['control'] = '#' . $this->controlId;
		}

		$options = Json::encode($this->pluginOptions);

		$js = 'jQuery("#' . $this->getId() . '").treeview(' . $options . ');';
		if ($this->selectParent) {
			$js .= "jQuery('#{$this->getId()} input[type=\"{$this->type}\"]').each (
				function () {
					$(this).bind('click change', function (){
						if($(this).is(':checked')) {
							$(this).parents('ul').siblings('input[type=\"{$this->type}\"]').attr('checked', 'checked');
						} else {
							$(this).siblings('ul').find('input[type=\"{$this->type}\"]').removeAttr('checked', 'checked');
						}
					});
				}
			);";
		}

		if ($this->type != static::TYPE_LABEL) {
			$js .= "
				var li = jQuery('#{$this->getId()} input[type=\"{$this->type}\"]:checked'),
					childUl = li.parents('ul');
					li.data('checked', true);
				childUl.each(function() {
					var obj = jQuery(this).first('li');

					obj.removeClass('expandable').removeClass('lastExpandable')
						.addClass('collapsable').addClass('lastCollapsable');
					obj.first('ul').show();
				});";
		}

		if ($this->allowNull) {
			$dataContainer = '[data-tree-' . $this->getId() . ']';
			$js .= '
			$("' . $dataContainer . '").on("click", function(){
				if (jQuery(this).data(\'checked\')) {
					this.checked = false;
				}
				console.log(this.checked);
				jQuery(this).data(\'checked\', this.checked);
				jQuery(\'' . $dataContainer . '\').not(\':checked\').data(\'checked\', false);
			});';
		}
		$view = $this->getView();
		$view->registerJs($js);

		AIOTreeAsset::register($view);
	}

	/**
	 * parent tag show and close
	 */
	public function parentStart()
	{
		if ($this->parentShow) {
			$this->_html .= '<' . $this->parentTag;
			$this->displayHtmlAttr($this->parentClass, 'class');
			$this->displayHtmlAttr($this->parentId, 'id');
			$this->displayHtmlAttr($this->parentStyle, 'style');
			$this->displayHtmlAttr($this->parentHtmlOptions);
			$this->_html .= '>';
		}
	}

	/**
	 * parent tag show and close
	 */
	public function parentClose()
	{
		if ($this->parentShow) {
			$this->_html .= '</' . $this->parentTag . '>';
		}
	}

	/** Tree header **/
	public function showHeader()
	{
		if ($this->headerShow) {
			$this->_html .= '<div class="' . $this->headerClass . '">' . $this->header . '</div>';
		}
	}

	/**
	 * control of collapse and expand details
	 */
	public function renderControl()
	{
		if ($this->controlShow) {
			/** control tag start **/
			$this->_html .= '<' . $this->controlTag;
			$this->displayHtmlAttr($this->controlClass, 'class');
			$this->displayHtmlAttr($this->controlId, 'id');
			$this->displayHtmlAttr($this->controlStyle, 'style');
			$this->displayHtmlAttr($this->controlHtmlOptions);
			$this->_html .= '>';

			/** collapse and expand **/
			$this->_html .= '<a href="javascript:;" id="' . $this->labelCollapseId . '" class="' . $this->labelCollapseClass . '">';
			$this->_html .= ArrayHelper::getValue($this->controlLabel, 'collapse', $this->labelCollapse);
			$this->_html .= '</a>';
			$this->_html .= $this->controlDivider;
			$this->_html .= '<a href="javascript:;" id="' . $this->labelExpandId . '" class="' . $this->labelExpandClass . '">';
			$this->_html .= ArrayHelper::getValue($this->controlLabel, 'expand', $this->labelExpand);
			$this->_html .= '</a>';

			/** control tag close **/
			$this->_html .= '</' . $this->controlTag . '>';
		}
	}

	/** display id, class, style and more *
	 *
	 * @param        $params
	 * @param string $attr
	 */
	public function displayHtmlAttr($params, $attr = '')
	{
		if (is_string($params) && trim($params) != '') {
			$this->_html .= " " . $attr . "='" . $params . "' ";
		} else if (is_array($params)) {
			foreach ($params as $attr => $value) {
				$this->_html .= " " . $attr . "='" . $value . "' ";
			}
		}
	}
}

?>