<?php

namespace sim4nix\aiotree;

use yii\web\AssetBundle;

class AIOTreeAsset extends AssetBundle
{
	public $sourcePath = '@sim4nix/aiotree/assets';

	public $css = [
		'css/jquery.treeview.css'
	];
	public $js = [
		'js/jquery.treeview.js',
		'js/jquery.treeview.edit.js',
		'js/jquery.treeview.async.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\jui\JuiAsset'
	];
}