<?php

namespace sim4nix\aiotree;

use kartik\editable\Editable;
use kartik\grid\EditableColumn;
use yii\helpers\ArrayHelper;

class AIOTreeColumn extends EditableColumn
{
	public $action = '';
	public $data;
	public $type = AIOTree::TYPE_RADIO;
	public $allowNull = false;

	public function renderDataCellContent($model, $key, $index)
	{
		$editableOptions = $this->editableOptions;

		if (!empty($editableOptions) && $editableOptions instanceof \Closure) {
			$editableOptions = call_user_func($editableOptions, $model, $key, $index, $this);
		}

		$editableOptions['inputType']             = Editable::INPUT_WIDGET;
		$editableOptions['widgetClass']           = AIOTree::className();
		$options                                  = ArrayHelper::getValue($editableOptions, 'options', []);
		$editableOptions['options']               = ArrayHelper::merge(
			$options,
			[
				'data'      => $this->data,
				'type'      => $this->type,
				'allowNull' => $this->allowNull,
			]
		);
		$editableOptions['formOptions']['action'] = $this->action;

		$this->editableOptions = $editableOptions;

		return parent::renderDataCellContent($model, $key, $index);
	}
}
